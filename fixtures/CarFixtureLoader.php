<?php

namespace App\Model\Entity;

use App\Model\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class CarFixtureLoader implements FixtureInterface {


	/**
	 * @var ObjectManager $manager
	 */
	public function load(ObjectManager $manager)
	{

		$car = new Car('Honda', 'Civic', 60000, 2002);
		$manager->persist($car);
		$car = new Car('Honda', 'Accord', 6500, 2016);
		$manager->persist($car);
		$car = new Car('Volswagen', 'Polo', 280000, 2002);
		$manager->persist($car);
		$car = new Car('Skoda', 'Octavia', 195300, 1999);
		$manager->persist($car);

		$manager->flush();
	}


}