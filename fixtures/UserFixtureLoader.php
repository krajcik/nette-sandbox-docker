<?php

namespace Fixtures;

use App\Model\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class UserFixtureLoader implements FixtureInterface {


    /**
     * @var ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = new User('John', 'Doe', 'password');
        $manager->persist($user);
        $manager->flush();
    }


}