# Docker

## Url

Application is available on url **localhost:8080**

## Commands

### Build

#### Dev version

Build application for developers
```bash
docker-compose -f .docker/docker-compose-dev.yml up -d --build
```

#### Prod version

```bash
docker-compose -f .docker/docker-compose-prod.yml up -d --build
```

## Other docker commands

Show all containers
```bash
docker ps
```

Login to bash of container
```bash
docker exec -it <container_hash> bash
```

Stop all containers
```bash
docker stop $(docker ps -aq)
```

Build without cache
```bash
docker-compose -f .docker/docker-compose-prod.yml build --no-cache
```

Remove all containers
```bash
docker rm $(docker ps -a -q)
```

Remove all images
```bash
docker rmi $(docker images -q)
```

