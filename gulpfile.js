'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

sass.compiler = require('node-sass');

gulp.task('sass', function () {
	return gulp.src('./www/assets/scss/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./www/assets/dist/css'));
});

gulp.task('sass:watch', function () {
	gulp.watch('.//www/assets/scss/**/*.scss', ['sass']);
});


var concat = require('gulp-concat');
gulp.task('js', function() {
	return gulp.src([
		"node_modules/jquery/dist/jquery.js",
		"node_modules/bootstrap/dist/js/bootstrap.min.js",
		"node_modules/nette.ajax.js/nette.ajax.js",
		"www/assets/js/*.js"
	])
		.pipe(concat('script.js'))
		.pipe(gulp.dest('www/assets/dist/js'));
});


var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');

gulp.task('cssmin', ['sass'], function () {
	gulp.src('www/assets/dist/css/style.css')
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('www/assets/dist/css'));
});

var uglify = require('gulp-uglify');
var pump = require('pump');

gulp.task('uglify', ['js'], function (cb) {
	pump([
			gulp.src('www/assets/dist/js/script.js'),
			uglify(),
			rename({suffix: '.min'}),
			gulp.dest('www/assets/dist/js')
		],
	);
});


gulp.task('fonts', function() {
	return gulp.src('node_modules/font-awesome/fonts/*')
		.pipe(gulp.dest('www/assets/fonts'))
})

gulp.task('dist', [ 'sass', 'cssmin', 'js', 'uglify', 'fonts' ]);