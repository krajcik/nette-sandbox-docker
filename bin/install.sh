#!/usr/bin/env bash
composer install
npm install
gulp dist

APP_CONTAINER_ID=$(docker ps | grep "docker_app_dev" | cut -f 1 -d " ")

CMD[0]="docker exec -it $APP_CONTAINER_ID bash -c \"php www/index.php orm:schema-tool:update --force\"" # update database schema
CMD[1]="docker exec -it $APP_CONTAINER_ID bash -c \"php www/index.php rabbitmq:setup-fabric\"" # update rabbit setup
CMD[2]="docker exec -it $APP_CONTAINER_ID bash -c \"pkill -f supervisord\"" # kill supervisor
CMD[3]="docker exec -it $APP_CONTAINER_ID bash -c \"php www/index.php doctrine:fixtures:load\"" # load data fixtures
CMD[4]="pkill supervisord" # Stop supervisor
CMD[4]="docker exec -it $APP_CONTAINER_ID bash -c \"supervisord -c .docker/supervisord.conf\"" # run supervisor

OS=$(uname)
if [[ $OS == "Linux" ]]; then
    CMD[5]="sudo chmod 777 -R temp/cache"
fi


for COMMAND in "${CMD[@]}"
do
   :
   eval $COMMAND
done





