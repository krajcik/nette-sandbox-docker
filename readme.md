Nette sandbox Web Project in docker
=================

This is a simple application using the [Nette](https://nette.org) in docker.

Docker containers
------------

- Apache with PHP
- Mysql
- Redis
- RabbitMQ


Requirements
------------

- For production version

  - Docker

- For developers version

  - PHP 5.6 or higher.
  - Docker
  - npm
  - Composer
  - Gulp


Installation
------------

- Production version

  - Build project
	```sh
	docker-compose -f .docker/docker-compose-prod.yml up -d --build
	```
- Developers version
  - Build project
	```sh
	docker-compose -f .docker/docker-compose-dev.yml up -d --build
	```
  - Install dependencies
	```bash
	bash bin/install.sh
	```

Run application
----------------

After installation visit `http://localhost:8080` in your browser to see the main page.

### Developers version of build

- Adminer is available on `http://localhost::8081`
  - server: *mysql*
  - username: *root*
  - password: *root*
- RabbitMQ management is available on `http://localhost:15672`
  - username: *guest*
  - password: *guest*
