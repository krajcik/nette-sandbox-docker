<?php

namespace App\Presenters;

use Kdyby\Doctrine\EntityManager;
use Nette;

class BasePresenter extends Nette\Application\UI\Presenter
{

	/** @var EntityManager @inject */
	public $entityManager;

	/** @var Nette\Security\IAuthenticator @inject */
	public $authenticator;

	protected function startup()
	{
		parent::startup();

		// log in user by fake authenticator
		$identity = $this->authenticator->authenticate(['id' => 1]);
		$this->user->login($identity);
	}


	protected function shutdown($response)
	{
		$this->entityManager->flush();
		parent::shutdown($response);
	}

}