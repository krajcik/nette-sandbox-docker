<?php

namespace App\Presenters;

use App\Components\ILogListFactory;
use App\Components\IPageFormFactory;
use App\Components\IPageListFactory;
use App\Model\Entity\Page;
use App\Model\Repository\PageRepository;

final class HomepagePresenter extends BasePresenter {

	/** @var IPageFormFactory @inject */
	public $pageFormFactory;

	/** @var PageRepository @inject */
	public $pageRepository;

	/** @var IPageListFactory @inject */
	public $pageListFactory;

	/** @var ILogListFactory @inject */
	public $logListFactory;


	public function renderDefault()
	{
		$this->template->pages = $this->pageRepository->findAll();
	}


	/**
	 * @return \App\Components\PageForm
	 */
	protected function createComponentPageForm()
	{
		$pageForm = $this->pageFormFactory->create();
		$pageForm->onSave[] = function(Page $page) {
			$this->flashMessage('Page was saved', 'success');
			$this->redirect('this');
		};
		return $pageForm;
	}


	/**
	 * @return \App\Components\PageList
	 */
	protected function createComponentPageList()
	{
		$pageList = $this->pageListFactory->create();
		$pageList->onRemove[] = function() {
			$this->flashMessage('Page was removed', 'success');
			$this->redrawControl('flashes');
		};
		return $pageList;
	}


	/**
	 * @return \App\Components\LogList
	 */
	protected function createComponentLogList()
	{
		return $this->logListFactory->create();
	}




}
