<?php

namespace App\Presenters;

use App\Model\Repository\CarRepository;

class TempApiPresenter extends BasePresenter {


	/** @var CarRepository @inject */
	public $carRepository;


	/**
	 * http://localhost:8080/temp-api/available-cars
	 * @throws \Nette\Application\AbortException
	 * @throws \ReflectionException
	 */
	public function actionAvailableCars() {
		$cars = $this->carRepository->findAllCars();

		$data = [];
		foreach ($cars as $car) {
			$data[$car->getId()] = $car->toPropertyArray();
		}

		$this->sendJson($data);
	}


}