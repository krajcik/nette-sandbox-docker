<?php

namespace App\Presenters;

use App\Model\SmartObject;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;

class FakeAuthenticator extends SmartObject implements IAuthenticator
{

	/**
	 * Performs an authentication against e.g. database.
	 * and returns IIdentity on success or throws AuthenticationException
	 *
	 * @param array $credentials
	 * @return IIdentity
	 */
	public function authenticate(array $credentials)
	{
		return new Identity($credentials['id'], NULL);
	}
}