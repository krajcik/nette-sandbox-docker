<?php

namespace App\Model\Entity;

use App\Model\SmartObject;
use Doctrine\ORM\Mapping as ORM;
use Nette\NotImplementedException;
use ReflectionClass;

class Entity extends SmartObject {

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return array
	 * @throws \ReflectionException
	 */
	public function toPropertyArray(): array
	{
		$data = array();
		$reflection = new ReflectionClass(static::class);
		foreach ($reflection->getProperties() as $property) {
			$property->setAccessible(true);
			$value = $property->getValue($this);
			if ($value instanceof Entity) {
				$data[$property->getName()] = $value->getId();
			} elseif (is_scalar($value)) {
				$data[$property->getName()] = $value;
			} else {
				throw new NotImplementedException('Unexpected type of value');
			}
		}
		return $data;
	}


}