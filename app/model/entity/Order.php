<?php

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany as OneToMany;
/**
 * @ORM\Entity
 * @ORM\Table(name="`order`")
 *
 */
class Order extends Entity {

	/**
	 * @ORM\Column(type="string",  nullable=false, length=128)
	 * @var string
	 */
	protected $firstName;

	/**
	 * @ORM\Column(type="string",  nullable=false, length=128)
	 * @var string
	 */
	protected $lastName;

	/**
	 * @var Car[]|ArrayCollection
 	 * @OneToMany(targetEntity="Car", mappedBy="order")
	 */
	protected $cars;

	/**
	 * @return string
	 */
	public function getFirstName(): string
	{
		return $this->firstName;
	}

	/**
	 * @param string $firstName
	 * @return static
	 */
	public function setFirstName(string $firstName): self
	{
		$this->firstName = $firstName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLastName(): string
	{
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 * @return static
	 */
	public function setLastName(string $lastName): self
	{
		$this->lastName = $lastName;
		return $this;
	}


	/**
	 * @param Car $car
	 * @return static
	 */
	public function addCar(Car $car): self
	{
		$this->cars->add($car);
		return $this;
	}


	/**
	 * @return ArrayCollection|Car[]
	 */
	public function getCars(): ArrayCollection
	{
		return $this->cars;
	}

}