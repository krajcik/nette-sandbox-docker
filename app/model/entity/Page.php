<?php

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany as OneToMany;
use Doctrine\ORM\Mapping\ManyToOne as ManyToOne;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repository\PageRepository")
 */
class Page extends Entity {

	/**
	 * @ORM\Column(type="string", length=55, nullable=false)
	 */
	protected $title;

	/**
	 * @ORM\Column(type="text", nullable=false)
	 */
	protected $content;


	/**
	 * @var PageMetaData[]|ArrayCollection
	 * @OneToMany(targetEntity="PageMetaData", mappedBy="page", fetch="EAGER", cascade={"persist", "remove"})
	 */
	protected $metaData;

	/**
	 * @ManyToOne(targetEntity="\App\Model\Entity\User", fetch="EAGER", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
	 * @var User
	 */
	protected $creator;


	/**
	 * Page constructor.
	 *
	 * @param string $title
	 * @param string $content
	 */
	public function __construct(string $title, string $content)
	{
		$this->title = $title;
		$this->content = $content;
		$this->metaData = new ArrayCollection();
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title)
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getContent(): string
	{
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent(string $content)
	{
		$this->content = $content;
	}

	/**
	 * @return PageMetaData[]|ArrayCollection
	 */
	public function getAllMetaData()
	{
		return $this->metaData;
	}

	/**
	 * @param PageMetaData $metaData
	 * @return self
	 */
	public function addMetaData(PageMetaData $metaData)
	{
		$this->metaData->add($metaData);
		return $this;
	}

	/**
	 * @internal
	 * @param User $creator
	 * @return Page
	 */
	public function setCreator(User $creator): Page
	{
		$this->creator = $creator;
		return $this;
	}

	/**
	 * @return User
	 */
	public function getCreator(): User
	{
		return $this->creator;
	}

	/**
	 * @param string $identification
	 * @return PageMetaData|null
	 */
	public function getMetaData(string $identification):? PageMetaData
	{
		$criteria = Criteria::create()
			->where(Criteria::expr()->eq('identification', $identification));

		return $this->getAllMetaData()->matching($criteria)->first();
	}

}