<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne as ManyToOne;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repository\PageRepository")
 */
class PageMetaData extends Entity {

	const AUTHOR_IDENTIFICATION = 'author';
	const DESCRIPTION_IDENTIFICATION = 'description';
	const KEYWORDS_IDENTIFICATION = 'keywords';


	/**
	 * @var string
	 * @ORM\Column(type="string", length=255, nullable=false)
	 */
	protected $value;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=32, nullable=false)
	 */
	protected $identification;

	/**
	 * @ManyToOne(targetEntity="\App\Model\Entity\Page", fetch="EAGER", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
	 * @var Page
	 */
	protected $page;


	/**
	 * PageMetaData constructor.
	 *
	 * @param Page $page
	 * @param string $identification
	 * @param string $value
	 */
	public function __construct(Page $page, string $identification, string $value)
	{
		$this->value = $value;
		$this->page = $page;
		$this->identification = $identification;
	}

	/**
	 * @return Page
	 */
	public function getPage(): Page
	{
		return $this->page;
	}

	/**
	 * @return string
	 */
	public function getValue(): string
	{
		return $this->value;
	}

	/**
	 * @param string $value
	 * @return self
	 */
	public function setValue(string $value): self
	{
		$this->value = $value;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getIdentification(): string
	{
		return $this->identification;
	}

}