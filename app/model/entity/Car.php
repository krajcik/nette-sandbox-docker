<?php

namespace App\Model\Entity;

use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Car extends Entity {

	/**
	 * @ORM\Column(type="string",  nullable=false, length=128)
	 * @var string
	 */
	protected $vendor;

	/**
	 * @ORM\Column(type="string",  nullable=false, length=128)
	 * @var string
	 */
	protected $model;

	/**
	 * @ORM\Column(type="integer",  nullable=false, precision=11)
	 * @var int
	 */
	protected $km;

	/**
	 * @ORM\Column(type="integer",  nullable=false, precision=11)
	 * @var int
	 */
	protected $year;


	/**
	 * Car constructor.
	 * @param string $vendor
	 * @param string $model
	 * @param int $km
	 * @param int $year
	 */
	public function __construct(string $vendor, string $model, int $km, int $year)
	{
		$this->vendor = $vendor;
		$this->model = $model;
		$this->km = $km;
		$this->year = $year;
	}

	/**
	 * @return string
	 */
	public function getVendor(): string {
		return $this->vendor;
	}

	/**
	 * @param string $vendor
	 * @return static
	 */
	public function setVendor(string $vendor)
	{
		$this->vendor = $vendor;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getModel(): string
	{
		return $this->model;
	}

	/**
	 * @param string $model
	 * @return static
	 */
	public function setModel(string $model): self
	{
		$this->model = $model;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getKm(): int
	{
		return $this->km;
	}

	/**
	 * @param int $km
	 * @return static
	 */
	public function setKm(int $km)
	{
		$this->km = $km;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getYear(): int
	{
		return $this->year;
	}

	/**
	 * @param int $year
	 * @return static
	 */
	public function setYear(int $year): self
	{
		$this->year = $year;
		return $this;
	}

}
