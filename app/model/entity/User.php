<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repository\UserRepository")
 */
class User extends Entity {


	/**
	 * @ORM\Column(type="string",  nullable=false, length=128)
	 * @var string
	 */
	protected $firstName;

	/**
	 * @ORM\Column(type="string",  nullable=false, length=128)
	 * @var string
	 */
	protected $lastName;

	/**
	 * @ORM\Column(type="string",  nullable=false, length=256)
	 * @var string
	 */
	protected $password;


	/**
	 * User constructor.
	 * @param string $firstName
	 * @param string $lastName
	 * @param string $password
	 */
	public function __construct(string $firstName, string $lastName, string $password)
	{
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	public function getFirstName(): string
	{
		return $this->firstName;
	}

	/**
	 * @param string $firstName
	 * @return static
	 */
	public function setFirstName(string $firstName): self
	{
		$this->firstName = $firstName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLastName(): string
	{
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 * @return static
	 */
	public function setLastName(string $lastName): self
	{
		$this->lastName = $lastName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 * @return static
	 */
	public function setPassword(string $password): self
	{
		$this->password = $password;
		return $this;
	}

}