<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Model\Repository\LogRepository")
 */
class Log extends Entity {

	/**
	 * @ORM\Column(type="string")
	 */
	protected $message;

	/**
	 * @ORM\Column(type="datetime", nullable=false)
	 */
	protected $inserted;


	/**
	 * Log constructor.
	 *
	 * @param string $message
	 */
	public function __construct(string $message)
	{
		$this->message = $message;
		$this->inserted = new \DateTime();
	}

	/**
	 * @return string
	 */
	public function getMessage(): string
	{
		return $this->message;
	}

	/**
	 * @param string $message
	 * @return self
	 */
	public function setMessage(string $message)
	{
		$this->message = $message;
		return $this;
	}


	/**
	 * @return \DateTime
	 */
	public function getInserted(): \DateTime
	{
		return $this->inserted;
	}




}