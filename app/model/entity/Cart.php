<?php

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne as ManyToOne;
use Doctrine\ORM\Mapping\OneToMany as OneToMany;

use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 */
class Cart extends Entity {

	/**
	 * @var User
	 * @ManyToOne(targetEntity="App\Model\Entity\User", fetch="EAGER", cascade={"persist"})
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $user;

	/**
	 * @var Car[]|ArrayCollection
	 * @OneToMany(targetEntity="Car", mappedBy="cart")
	 */
	protected $cars;


	/**
	 * Cart constructor.
	 * @param User $user
	 * @param ArrayCollection $cars
	 */
	public function __construct(User $user, ArrayCollection $cars)
	{
		$this->user = $user;
		$this->cars = $cars;
	}

	/**
	 * @param Car $car
	 * @return static
	 */
	public function addCar(Car $car): self
	{
		$this->cars->add($car);
		return $this;
	}

	/**
	 * @return Car[]|ArrayCollection
	 */
	public function getCars(): ArrayCollection
	{
		return $this->cars;
	}

}