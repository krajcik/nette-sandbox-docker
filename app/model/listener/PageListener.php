<?php

namespace App\Model\Listener;

use App\Model\Entity\Log;
use App\Model\Entity\Page;
use App\Model\Queue\LogProducer;
use App\Model\Repository\LogRepository;
use Kdyby\Events\Subscriber;
use Tracy\Debugger;

class PageListener implements Subscriber {

	/** @var LogProducer */
	protected $logProducer;


	/**
	 * PageListener constructor.
	 *
	 * @param LogProducer $logProducer
	 */
	public function __construct(LogProducer $logProducer)
	{
		$this->logProducer = $logProducer;
	}


	/**
	 * Returns an array of events this subscriber wants to listen to.
	 *
	 * @return string[]
	 */
	public function getSubscribedEvents()
	{
		return [
			'App\Model\Repository\PageRepository::onPersist'
		];
	}

	public function onPersist(Page $page)
	{
		$this->logProducer->log("Page '" . $page->getTitle() . "' was saved'");
	}

}