<?php

namespace App\Model;

use App\Model\Entity\Page;
use App\Model\Repository\PageMetaDataRepository;
use App\Model\Repository\PageRepository;
use Doctrine\DBAL\Driver\PDOException;
use Kdyby\Doctrine\EntityManager;
use Tracy\Debugger;

class PageFacade extends SmartObject {

	/** @var PageRepository */
	protected $pageRepository;

	/** @var PageMetaDataRepository */
	protected $pageMetaDataRepository;

	/** @var EntityManager */
	protected $entityManager;

	/** @var CurrentUser */
	protected $currentUser;


	/**
	 * PageFacade constructor.
	 *
	 * @param PageRepository $pageRepository
	 * @param PageMetaDataRepository $pageMetaDataRepository
	 * @param EntityManager $entityManager
	 * @param CurrentUser $currentUser
	 */
	public function __construct(
		PageRepository $pageRepository,
		PageMetaDataRepository $pageMetaDataRepository,
		EntityManager $entityManager,
		CurrentUser $currentUser
	) {
		$this->pageRepository = $pageRepository;
		$this->pageMetaDataRepository = $pageMetaDataRepository;
		$this->entityManager = $entityManager;
		$this->currentUser = $currentUser;
	}


	/**
	 * @param Page $page
	 * @return Page|null
	 */
	public function addPage(Page $page):? Page
	{
		$this->entityManager->beginTransaction();
		try {
			$page->setCreator($this->currentUser->getUser());
			$this->pageRepository->persist($page);
			$this->entityManager->commit();
		} catch(PDOException $e) {
			Debugger::log($e);
			$this->entityManager->rollback();
			return NULL;
		}
		return $page;
	}


	/**
	 * @param Page $page
	 * @return Page
	 */
	public function editPage(Page $page): Page {
		$this->pageRepository->persist($page);
		return $page;
	}


}
