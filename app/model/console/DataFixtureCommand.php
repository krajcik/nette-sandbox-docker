<?php

namespace App\Model\Console;

use Kdyby\Doctrine\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class DataFixtureCommand extends Command
{

    /** @var EntityManager */
    private $entityManager;


    /**
     * DataFixtureCommand constructor.
     * @param EntityManager $entityManager
     * @param string|null $name
     */
    public function __construct(EntityManager $entityManager, ?string $name = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }


    protected function configure(): void
    {
        $this->setName('doctrine:fixtures:load')
            ->setDescription('Load data fixtures');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $loader = new Loader();
        $loader->loadFromDirectory(VENDOR_DIR . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'fixtures');

        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->execute($loader->getFixtures());

        $output->writeLn('Fixtures are loaded');
        return 0; // zero return code means everything is ok
    }

}