<?php

namespace App\Model\Repository;

use App\Model\Entity\User;
use App\Model\SmartObject;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\Mapping\ClassMetadata;

class UserRepository extends SmartObject {

	/** @var EntityRepository */
	protected $entityRepository;

	/** @var EntityManager */
	protected $entityManager;


	/**
	 * PageRepository constructor.
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$classMetaData = new ClassMetadata(User::class);
		$this->entityRepository = new EntityRepository($em, $classMetaData);
		$this->entityManager = $em;
	}

	/**
	 * @param User $user
	 */
	public function persist(User $user)
	{
		$this->entityManager->persist($user);
	}


	/**
	 * @param int $id
	 * @return User
	 */
	public function find(int $id)
	{
		return $this->entityRepository->find($id);
	}


}