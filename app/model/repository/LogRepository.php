<?php

namespace App\Model\Repository;

use App\Model\Entity\Log;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\Mapping\ClassMetadata;
use Nette\SmartObject;

class LogRepository
{

	use SmartObject;

	/** @var EntityRepository */
	protected $entityRepository;

	/** @var EntityManager */
	protected $entityManager;

	/**
	 * PageRepository constructor.
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$classMetaData = new ClassMetadata(Log::class);
		$this->entityRepository = new EntityRepository($em, $classMetaData);
		$this->entityManager = $em;
	}


	/**
	 * Find all logs
	 * @return Log[]
	 */
	public function findAll()
	{
		return $this->entityRepository->findAll();
	}


	/**
	 * @param int $id
	 * @return null|Log
	 */
	public function find(int $id)
	{
		return $this->entityRepository->find($id);
	}


	/**
	 * @param Log $log
	 */
	public function persist(Log $log)
	{
		$this->entityManager->persist($log);
	}


}