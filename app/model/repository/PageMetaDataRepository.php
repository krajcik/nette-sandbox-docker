<?php

namespace App\Model\Repository;

use App\Model\Entity\PageMetaData;
use App\Model\SmartObject;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\Mapping\ClassMetadata;

class PageMetaDataRepository extends SmartObject {

	/** @var EntityRepository */
	protected $entityRepository;

	/** @var EntityManager */
	protected $entityManager;


	/**
	 * PageRepository constructor.
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$classMetaData = new ClassMetadata(PageMetaData::class);
		$this->entityRepository = new EntityRepository($em, $classMetaData);
		$this->entityManager = $em;
	}


	/**
	 * @param PageMetaData $metaData
	 */
	public function persist(PageMetaData $metaData) {
		$this->entityManager->persist($metaData);
	}

}
