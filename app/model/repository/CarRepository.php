<?php

namespace App\Model\Repository;

use App\Model\Entity\Car;
use Doctrine\Common\Collections\ArrayCollection;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\Mapping\ClassMetadata;

class CarRepository extends \App\Model\SmartObject {


	/** @var EntityRepository */
	protected $entityRepository;

	/** @var EntityManager */
	protected $entityManager;

	/**
	 * PageRepository constructor.
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$classMetaData = new ClassMetadata(Car::class);
		$this->entityRepository = new EntityRepository($em, $classMetaData);
		$this->entityManager = $em;
	}


	/**
	 * @return array|Car[]
	 */
	public function findAllCars(): array
	{
		return $this->entityRepository->findAll();
	}

}