<?php

namespace App\Model\Repository;

use App\Model\Entity\Page;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Kdyby\Doctrine\Mapping\ClassMetadata;
use Nette\SmartObject;

class PageRepository extends \App\Model\SmartObject {

	use SmartObject;

	/** @var array|callable (Page $page)  */
	public $onPersist = [];

	/** @var array|callable (Page $page)  */
	public $onRemove = [];

	/** @var EntityRepository */
	protected $entityRepository;

	/** @var EntityManager */
	protected $entityManager;

	/**
	 * PageRepository constructor.
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$classMetaData = new ClassMetadata(Page::class);
		$this->entityRepository = new EntityRepository($em, $classMetaData);
		$this->entityManager = $em;
	}


	/**
	 * @param int $id
	 * @return null|Page
	 */
	public function find(int $id) {
		return $this->entityRepository->find($id);
	}


	/**
	 * @param Page $page
	 */
	public function persist(Page $page) {
		$this->entityManager->persist($page);
		$this->onPersist($page);
	}


	/**
	 * @return Page[]
	 */
	public function findAll()
	{
		return $this->entityRepository->findAll();
	}


	/**
	 * @param Page $page
	 */
	public function remove(Page $page) {
		$this->entityManager->remove($page)
			->flush($page);
	}

}