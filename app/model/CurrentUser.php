<?php

namespace App\Model;

use App\Model\Repository\UserRepository;
use Nette\Security\User;

class CurrentUser extends SmartObject {

	/** @var User */
	protected $netteUser;

	/** @var UserRepository */
	protected $userRepository;


	/**
	 * CurrentUser constructor.
	 *
	 * @param User $netteUser
	 * @param UserRepository $userRepository
	 */
	public function __construct(User $netteUser, UserRepository $userRepository)
	{
		$this->netteUser = $netteUser;
		$this->userRepository = $userRepository;
	}


	/**
	 * @return Entity\User|null
	 */
	public function getUser():? \App\Model\Entity\User
	{
		$userId = $this->netteUser->getId();
		return $this->userRepository->find($userId);
	}

}