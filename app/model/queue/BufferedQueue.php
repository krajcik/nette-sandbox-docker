<?php

namespace App\Model\Queue;

use App\Model\SmartObject;

class BufferedQueue extends SmartObject {

	/** @var Task[]  */
	protected $queue = [];


	/**
	 * Add same callback to queue only once
	 * @param callable $callback
	 * @param array $args
	 * @return self
	 */
	public function add(callable $callback, array $args) {
		$this->addTask(new Task($callback, $args));
		return $this;
	}

	/**
	 * Add same callback to queue only once
	 * @param Task $task
	 * @return self
	 */
	public function addTask(Task $task) {
		$this->queue[$task->getIdentifier()] = $task;
		return $this;
	}


	/**
	 * Process all tasks
	 */
	public function process() {
		foreach ($this->queue as $task) {
			$task->process();
		}
	}

}
