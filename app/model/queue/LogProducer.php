<?php

namespace App\Model\Queue;

use Tracy\Debugger;

class LogProducer extends BufferedProducer {

	const ADD_ROUTING_KEY = 'log_add';


	/**
	 * @param string $message
	 * @return self
	 */
	public function log(string $message) {
		$this->sendToBuffer($this->createMsg($message), self::ADD_ROUTING_KEY);
		return $this;
	}


	/**
	 * @return string
	 */
	protected function getProducerName() {
		return 'log';
	}

}
