<?php

namespace App\Stock\Model\Queue\Producer;

use App\Model\Entity\Log;
use App\Model\Queue\BaseConsumer;
use App\Model\Repository\LogRepository;
use Kdyby\Doctrine\EntityManager;
use Kdyby\RabbitMq\IConsumer;
use PhpAmqpLib\Message\AMQPMessage;
use Tracy\Debugger;

class LogConsumer extends BaseConsumer implements IConsumer  {

	/** @var LogRepository */
	protected $logRepository;

	/** @var EntityManager */
	protected $entityManager;


	/**
	 * LogConsumer constructor.
	 *
	 * @param LogRepository $logRepository
	 * @param EntityManager $entityManager
	 */
	public function __construct(LogRepository $logRepository, EntityManager $entityManager) {
		$this->logRepository = $logRepository;
		$this->entityManager = $entityManager;
	}


	/**
	 * @param AMQPMessage $msg
	 * @return bool
	 * @throws \RuntimeException
	 * @throws \LogicException
	 */
	public function addLog(AMQPMessage $msg) {
		try {
			$message = $this->messageToArray($msg);
			$log = new Log($message['message']);
			$this->logRepository->persist($log);
			$this->entityManager->flush($log);
			return self::MSG_ACK;
		} catch (\Exception $e) {
			Debugger::log($e);
			return self::MSG_REJECT;
		}
	}


}