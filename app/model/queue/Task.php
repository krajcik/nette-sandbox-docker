<?php

namespace App\Model\Queue;

use App\Model\Entity\Entity;
use App\Model\SmartObject;
use Nette\Utils\Callback;

class Task extends SmartObject {

	/** @var string */
	protected $identifier;

	/** @var callable */
	protected $callback;

	/** @var array */
	protected $args;


	/**
	 * Task constructor.
	 * @param callable $callback
	 * @param array $args
	 */
	public function __construct(callable $callback, array $args) {
		$this->identifier = $this->generateIdentifier($callback, $args);
		$this->callback = $callback;
		$this->args = $args;
	}


	/**
	 * @return string
	 */
	public function getIdentifier() {
		return $this->identifier;
	}


	/**
	 * @return mixed
	 */
	public function process() {
		return Callback::invokeArgs($this->callback, $this->args);
	}


	/**
	 * @param callable $callback
	 * @param array $args
	 * @return string
	 */
	public function generateIdentifier(callable $callback, array $args) {
		$string = '';
		foreach ($callback as $arg) {
			if (is_object($arg)) {
				$string .= get_class($arg);
			} elseif (is_scalar($arg)) {
				$string .= (string) $arg;
			} else {
				throw new \UnexpectedValueException();
			}
		}

		foreach ($args as $arg) {
			if ($arg instanceof Entity) {
				$string .= $arg->getId() . '-' . get_class($arg);
			} elseif (is_scalar($arg)) {
				$string .= (string) $arg;
			} else {
				throw new \UnexpectedValueException();
			}
		}
		return md5($string);
	}

}