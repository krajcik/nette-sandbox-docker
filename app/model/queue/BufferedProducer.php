<?php

namespace App\Model\Queue;

use Kdyby\Events\Subscriber;
use Kdyby\RabbitMq\Connection;
use Kdyby\RabbitMq\Producer;

/**
 * Class BufferedProducer
 * @package App\Model\Queue
 */
abstract class BufferedProducer extends BaseProducer implements Subscriber {

	/** @var Producer */
	protected $producer;

	/** @var Connection */
	protected $rabbit;

	/** @var BufferedQueue */
	protected $bufferedQueue;


	/**
	 * MailProducer constructor.
	 *
	 * @param Connection $rabbit
	 */
	public function __construct(Connection $rabbit) {
		$this->rabbit = $rabbit;
		$this->producer = $this->rabbit->getProducer($this->getProducerName());
		$this->bufferedQueue = new BufferedQueue();
	}


	/**
	 * @return array
	 */
	public function getSubscribedEvents() {
		return [
			'Nette\Application\Application::onShutdown' => 'onShutdown',
		];
	}


	/**
	 * Flush all messages to rabbitMQ
	 * @return bool
	 */
	public function onShutdown() {
		$this->bufferedQueue->process();
		return TRUE;
	}


	/**
	 * @param array  $message
	 * @param string $routingKey
	 * @return static
	 */
	protected function sendToBuffer(array $message, string $routingKey = '') {
		$this->bufferedQueue->add([$this->producer, 'publish'], [json_encode($message), $routingKey]);
		return $this;
	}

}