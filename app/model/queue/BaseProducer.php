<?php

namespace App\Model\Queue;

abstract class BaseProducer extends \App\Model\SmartObject {

	/**
	 * @param string $message
	 * @return array
	 */
	protected function createMsg(string $message) {
		return [
			'message' => $message,
		];
	}


	/**
	 * @return string
	 */
	abstract protected function getProducerName();

}