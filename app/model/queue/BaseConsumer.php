<?php

namespace App\Model\Queue;

use App\Model\SmartObject;
use PhpAmqpLib\Message\AMQPMessage;

abstract class BaseConsumer extends SmartObject {

	/**
	 * @param AMQPMessage $msg
	 * @return array
	 */
	protected function messageToArray(AMQPMessage $msg) {
		$message = (array) json_decode($msg->body, TRUE);
		return $message;
	}


}