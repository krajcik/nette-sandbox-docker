<?php

define('VENDOR_DIR', __DIR__ . '/../vendor');

require VENDOR_DIR . '/autoload.php';

$configurator = new Nette\Configurator;

$envProduction = getenv('PRODUCTION_MODE');
$isProduction = FALSE;
if ($envProduction == 'true' || $envProduction == 1) {
	$isProduction = TRUE;
}

$configurator->setDebugMode(!$isProduction); // enable for your remote IP

$configurator->enableTracy(__DIR__ . '/../log');

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configLocalNeonPath = __DIR__ . '/config/config.local.neon';

if (file_exists($configLocalNeonPath)) {
	$configurator->addConfig($configLocalNeonPath);
}

$container = $configurator->createContainer();

return $container;
