<?php

namespace App\Components;

interface ILogListFactory
{

	/**
	 * @return LogList
	 */
	public function create(): LogList;

}
