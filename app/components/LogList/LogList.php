<?php

namespace App\Components;

use App\Model\Repository\LogRepository;
use Nette\Application\UI\Control;

class LogList extends Control {


	/** @var LogRepository */
	protected $logRepository;


	/**
	 * LogList constructor.
	 *
	 * @param LogRepository $logRepository
	 */
	public function __construct(LogRepository $logRepository)
	{
		parent::__construct();
		$this->logRepository = $logRepository;
	}


	/**
	 * Render
	 */
	public function render()
	{
		$this->template->logs = $this->logRepository->findAll();
		$this->template->setFile(__DIR__ . DIRECTORY_SEPARATOR . 'LogList.latte');
		$this->template->render();
	}

}