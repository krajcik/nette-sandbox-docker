<?php

namespace App\Components;

interface IPageFormFactory
{

	/**
	 * @return PageForm
	 */
	public function create(): PageForm;

}
