<?php

namespace App\Components;

use App\Model\Entity\Page;
use App\Model\Entity\PageMetaData;
use App\Model\PageFacade;
use App\Model\Repository\PageRepository;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class PageForm extends Control
{

	/** @var array|callable (Page $page) */
	public $onSave = [];

	/** @var Page|null */
	protected $page;

	/** @var PageFacade */
	protected $pageFacade;

	/** @var PageRepository */
	protected $pageRepository;


	/**
	 * PageForm constructor.
	 *
	 * @param PageRepository $pageRepository
	 * @param PageFacade $pageFacade
	 */
	public function __construct(PageRepository $pageRepository, PageFacade $pageFacade)
	{
		parent::__construct();
		$this->pageFacade = $pageFacade;
		$this->pageRepository = $pageRepository;
	}


	/**
	 * @param Page $page
	 * @return self
	 */
	public function setPage(Page $page)
	{
		$this->page = $page;
		return $this;
	}


	/**
	 * Render
	 */
	public function render()
	{
		$this->init();
		$this->template->render(__DIR__ . DIRECTORY_SEPARATOR . 'PageForm.latte');
	}


	/**
	 * @param string $name
	 * @return Form
	 */
	public function createComponentForm(string $name) {
		$form = new Form();
		$form->addHidden('id');
		$form->addText('title', 'Title')
			->setOption('description', 'Title of page')
			->setRequired();
		$form->addTextArea('content', 'Content')
			->setRequired();

		$form->addGroup('Seo');
		$form->addText('author', 'Author');
		$form->addTextArea('meta_description', 'Description');
		$form->addText('meta_keywords', 'Keywords');

		$form->addSubmit('submit', 'Save');
		$form->onSuccess[] = [$this, 'formSucceeded'];
		return $form;
	}


	/**
	 * @param Form $form
	 * @param ArrayHash $values
	 */
	public function formSucceeded(Form $form, ArrayHash $values)
	{
		if (!empty($values['id'])) {
			$page = $this->pageRepository->find($values['id']);
			$page = $this->editPage($page, $values);
		} else {
			$page = $this->addPage($values);
		}

		$this->onSave($page);
	}


	/**
	 * @param Page $page
	 * @param ArrayHash $values
	 * @return Page
	 */
	protected function editPage(Page $page, ArrayHash $values): Page
	{
		$page->setTitle($values['title']);
		$page->setContent($values['content']);

		if ($metaData = $page->getMetaData(PageMetaData::AUTHOR_IDENTIFICATION)) {
			$metaData->setValue($values['author']);
		} else {
			$page->addMetaData(new PageMetaData($page, PageMetaData::AUTHOR_IDENTIFICATION, $values['author']));
		}

		if ($metaData = $page->getMetaData(PageMetaData::DESCRIPTION_IDENTIFICATION)) {
			$metaData->setValue($values['meta_description']);
		} else {
			$page->addMetaData(new PageMetaData($page, PageMetaData::DESCRIPTION_IDENTIFICATION, $values['meta_description']));
		}

		if ($metaData = $page->getMetaData(PageMetaData::KEYWORDS_IDENTIFICATION)) {
			$metaData->setValue($values['meta_keywords']);
		} else {
			$page->addMetaData(new PageMetaData($page, PageMetaData::KEYWORDS_IDENTIFICATION, $values['meta_keywords']));
		}
		$this->pageFacade->editPage($page);
		return $page;
	}


	/**
	 * @param ArrayHash $values
	 * @return Page
	 */
	protected function addPage(ArrayHash $values): Page
	{
		$page = new Page(
			$values['title'],
			$values['content']
		);
		if ($values['author'] !== '') $page->addMetaData(new PageMetaData($page, PageMetaData::AUTHOR_IDENTIFICATION, $values['author']));
		if ($values['meta_description'] !== '') $page->addMetaData(new PageMetaData($page, PageMetaData::DESCRIPTION_IDENTIFICATION, $values['meta_description']));
		if ($values['meta_keywords'] !== '') $page->addMetaData(new PageMetaData($page, PageMetaData::KEYWORDS_IDENTIFICATION, $values['meta_keywords']));

		$this->pageFacade->addPage($page);
		return $page;
	}


	/**
	 * Init page
	 */
	protected function init()
	{
		if (!$this->page) return;
		$values = [
			'id' => $this->page->getId(),
			'title' => $this->page->getTitle(),
			'content' => $this->page->getContent(),
		];

		if ($meta = $this->page->getMetaData(PageMetaData::AUTHOR_IDENTIFICATION)) {
			$values['author'] = $meta->getValue();
		}

		if ($meta = $this->page->getMetaData(PageMetaData::DESCRIPTION_IDENTIFICATION)) {
			$values['meta_description'] = $meta->getValue();
		}

		if ($meta = $this->page->getMetaData(PageMetaData::KEYWORDS_IDENTIFICATION)) {
			$values['meta_keywords'] = $meta->getValue();
		}


		$this['form']->setDefaults($values);
	}

}