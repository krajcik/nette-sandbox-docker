<?php

namespace App\Components;


interface IPageListFactory
{

	public function create(): PageList;

}
