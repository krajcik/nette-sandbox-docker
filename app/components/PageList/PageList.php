<?php

namespace App\Components;

use Nette\Application\BadRequestException;

class PageList extends \Nette\Application\UI\Control
{
	/** @var array|callable  */
	public $onRemove = [];

	/** @var \App\Model\Repository\PageRepository */
	protected $pageRepository;

	/** @var IPageFormFactory */
	protected $pageFormFactory;


	/**
	 * PageList constructor.
	 *
	 * @param \App\Model\Repository\PageRepository $pageRepository
	 * @param IPageFormFactory $pageFormFactory
	 */
	public function __construct(
		\App\Model\Repository\PageRepository $pageRepository,
		IPageFormFactory $pageFormFactory
	) {
		parent::__construct();
		$this->pageRepository = $pageRepository;
		$this->pageFormFactory = $pageFormFactory;
	}


	/**
	 * Render page list
	 */
	public function render() {
		$this->template->pages = $this->pageRepository->findAll();
		$this->template->render(__DIR__ . DIRECTORY_SEPARATOR . 'PageList.latte');
	}


	/**
	 * Remove page
	 *
	 * @param int $id
	 * @throws BadRequestException
	 */
	public function handleRemove($id) {
		$page = $this->pageRepository->find($id);
		if (!$page) {
			throw new BadRequestException();
		}
		$this->pageRepository->remove($page);
		$this->redrawControl('pageList');
		$this->onRemove();
	}


	/**
	 * @param int $id
	 * @throws BadRequestException
	 */
	public function handleShowEditForm($id) {
		$page = $this->pageRepository->find($id);
		if (!$page) {
			throw new BadRequestException();
		}
		$this['pageForm']->setPage($page);
		$this->template->showModal = TRUE;
		$this->redrawControl('pageForm');
		$this->redrawControl('script');
	}


	/**
	 * @return PageForm
	 */
	protected function createComponentPageForm() {
		return $this->pageFormFactory->create();
	}

}