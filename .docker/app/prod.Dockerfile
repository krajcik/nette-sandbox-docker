FROM php:7.2-apache
RUN pecl install redis-4.0.1 \ && pecl install xdebug-2.6.0 \ && docker-php-ext-enable redis xdebug

RUN apt-get update
RUN apt-get install -y libpng-dev # is required for install gd
RUN apt-get install -y zlib1g-dev # is required for install zip
RUN apt-get install -y unzip

RUN docker-php-ext-install pdo_mysql zip gd bcmath
COPY ./.docker/app/php.ini /usr/local/etc/php/conf.d/php-custom.ini

# allow apache modules
RUN a2enmod rewrite

# Install nodejs (and npm)
RUN apt-get install -my wget gnupg
RUN apt-get install --yes curl
RUN curl --silent --location https://deb.nodesource.com/setup_11.x | bash -
RUN apt-get install --yes nodejs
RUN apt-get install --yes build-essential

# Install git
RUN apt-get install git -y

# Install netcat for check if service from other container is ready
RUN apt-get install netcat -y

# Install supervisor for rabbitmq consumers and producers
RUN apt-get install supervisor -y

# Install composer
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer
RUN composer global require hirak/prestissimo --no-plugins --no-scripts # spiffing plugin for Composer which parallelises all the downloads

# Install project
RUN git clone https://ond_ej_kraj_k@bitbucket.org/krajcik/nette-sandbox-docker.git .
COPY ./.docker/app/config.local.neon /var/www/html/app/config/config.local.neon
RUN sh ./.docker/app/sh/install-project.sh
