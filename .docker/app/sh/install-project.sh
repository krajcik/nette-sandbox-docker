#!/usr/bin/env bash
composer install
mkdir temp/cache
npm install
npm install gulp-cli -g
gulp dist
sh .docker/app/sh/fix-permission.sh
