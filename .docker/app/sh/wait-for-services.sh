#!/usr/bin/env bash
echo Waiting for redis service start...;

while ! nc -z redis 6379;
do
sleep 1;
done;

echo Waiting for mysql service start...;

while ! nc -z mysql 3306;
do
sleep 1;
done;

echo Waiting for rabbitmq service start...;

while ! nc -z rabbit 5672;
do
sleep 1;
done;

echo Connected!;
