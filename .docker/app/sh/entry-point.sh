#!/usr/bin/env bash
set -e # exit script if any command fails (non-zero value)
sh .docker/app/sh/wait-for-services.sh
php www/index.php orm:schema-tool:update --force
php www/index.php doctrine:fixtures:load # load data fixtures
sh .docker/app/sh/fix-permission.sh # by previous command was created files in "temp/cache" and must be set permission again
echo "[SUCCESS] Database schema was generated";
php www/index.php rabbitmq:setup-fabric
echo "[SUCCESS] Rabbit setup done";
supervisord -c .docker/supervisord.conf
echo "[SUCCESS] Run supervisor done";
exec "$@"