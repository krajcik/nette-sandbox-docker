FROM php:7.2-apache
RUN pecl install redis-4.0.1 \ && pecl install xdebug-2.6.0 \ && docker-php-ext-enable redis xdebug

RUN apt-get update
RUN apt-get install -y libpng-dev # is required for install gd
RUN apt-get install -y zlib1g-dev # is required for install zip
RUN apt-get install -y unzip

RUN docker-php-ext-install pdo_mysql zip gd bcmath
#COPY ./.docker/app/php.ini /usr/local/etc/php/conf.d/php-custom.ini

# Install supervisor for rabbitmq consumers and producers
RUN apt-get install supervisor -y
RUN a2enmod rewrite